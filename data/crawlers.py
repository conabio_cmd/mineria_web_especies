#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-
import sys

import argparse
import collections
import configparser
import logging
import numpy
import os
import pandas
import pydash
import requests
import time
import uuid

import loader

from bs4 import BeautifulSoup

from pdb import set_trace as bp

Crawler = loader.Crawler


class CrawlerHindawi(Crawler):
    uri = "https://www.hindawi.com/search/all/{{1}}/and/journal/{{2}}/{{3}}"
    journals = [
        "Advances in Agriculture",
        "Advances in Bioinformatics",
        "International Journal of Agronomy",
        "International Journal of Ecology",
        "International Journal of Forestry Research",
        "International Journal of Zoology",
        "Journal of Marine Biology"
    ]
    crawled = set()

    def __init__(self, log_path):
        self.log_path = log_path

    def crawl(self, token):
        all_docs = []

        for journal in self.journals:
            journal = journal.lower()
            has_next = True
            start = 1

            while has_next:
                u = self.uri\
                    .replace("{{1}}", token.lower().replace(" ", "+"))\
                    .replace("{{2}}", journal.lower().replace(" ", "+"))\
                    .replace("{{3}}", str(start))

                docs, has_next = self.crawl_search_results(u, start=start)
                all_docs += docs

                start += 1

        return all_docs

    def crawl_search_results(self, uri, start=1):
        base_download_uri = "http://downloads.hindawi.com{{1}}"

        response = self.get_response(uri, encoding="utf-8")

        if response == {}:
            return [], False

        soup = BeautifulSoup(response["data"])
        docs = []

        try:
            resdiv = soup.body.find_all("div", {"id": "SearchResultPnl"})[0]
            lis = resdiv.find_all("li")

            if len(lis) == 0:
                return [], False

            for li in lis:
                temp = li.find("a", href=True)

                doc_url = temp["href"]
                if doc_url not in self.crawled:
                    title = " ".join([str(c) for c in temp.contents])

                    id_uri = "/".join(doc_url.split("/")[0:-1])

                    docs.append(
                        [str(uuid.uuid4()),
                         doc_url,
                         title,
                         base_download_uri.replace("{{1}}", id_uri+".xml")]
                    )

                self.crawled(doc_url)
        except:
            pass

        return docs, True


class CrawlerPLOS(Crawler):
    rows = 50
    uri = 'http://api.plos.org/search?q=body:"{{1}}"%20AND%20journal:"{{2}}"&fl=id,title,elocation_id&start={{3}}&rows={{4}}'
    download_uri = "https://journals.plos.org/plosone/article/file?id={{1}}&type=manuscript"
    journals = [
        "plos biology",
        "plos computational biology",
        # "plos one",
    ]
    crawled = set()

    def __init__(self, log_path):
        self.log_path = log_path

    def crawl(self, token):
        all_docs = []
        has_next = True
        start = 0

        for journal in self.journals:

            while has_next:
                uri = self.uri\
                    .replace("{{1}}", token.lower())\
                    .replace("{{2}}", journal.lower())\
                    .replace("{{3}}", str(start))\
                    .replace("{{4}}", str(self.rows))

                response = self.get_response(
                    uri, type="json", encoding="utf-8")

                try:
                    response = response["data"]["response"]

                    total = response["numFound"]

                    if total == 0:
                        return []

                    current = response["start"]

                    has_next = (current + self.rows) < total
                    start += self.rows

                    docs = response["docs"]
                    for doc in docs:
                        if doc["id"] not in self.crawled:
                            all_docs.append([str(uuid.uuid4()),
                                             doc["id"],
                                             doc["title"],
                                             self.download_uri.replace("{{1}}", doc["id"])])

                        self.crawled.add(doc["id"])
                except:
                    pass

        return all_docs


def crawl(file_path, dest_path):
    tokens = []
    with open(file_path, mode="r") as _f:
        lines = _f.read()
        lines = lines.lower()
        lines = lines.split("\n")

    tokens = pydash.chain(lines)\
        .filter(lambda x: len(x) > 2)\
        .map(lambda x: x.lower().strip())\
        .value()
    docs_df = pandas.DataFrame()

    cp = CrawlerPLOS("")
    ch = CrawlerHindawi("")

    for token in tokens:
        try:
            logger = logging.getLogger("CRAWL")
            logger.info('@{}'.format(token))

            token_to_store = token.replace(" ", "_")
            documents = cp.crawl(token)
            logger.info('FOUND {} IN PLOS'.format(str(len(documents))))

            if len(documents) > 0:
                df = pandas.DataFrame(numpy.asarray(documents),
                                      columns=["id", "doc_id", "title", "download"])
                df["token"] = token_to_store
                df["source"] = "PLOS"

                docs_df = pandas.concat([docs_df, df])

            documents = ch.crawl(token)
            logger.info('FOUND {} IN HINDAWI'.format(str(len(documents))))
            if len(documents) > 0:
                df = pandas.DataFrame(numpy.asarray(documents),
                                      columns=["id", "doc_id", "title", "download"])
                df["token"] = token_to_store
                df["source"] = "Hindawi"

                docs_df = pandas.concat([docs_df, df])
        except:
            bp()
            print(sys.exc_info())

    docs_df.to_csv(os.path.join(dest_path, "docs.csv"),
                   sep=',',
                   encoding='utf-8')


def download(file_path, dest_path):
    df = pandas.read_csv(file_path)

    curr_file = 1
    prev_species = ""
    total = df.shape
    curr = 0

    for ix, row in df[["id", "download", "token", "source"]].iterrows():
        sp = row["token"]
        url = row["download"]
        file_id = row["id"]
        curr += 1

        if prev_species != sp:
            curr_file = 1

        response = requests.get(url, allow_redirects=False)
        file_dest = os.path.join(dest_path, str(file_id)+".xml")

        if response.status_code == 200:
            with open(file_dest, "wb") as dest_file:
                dest_file.write(response.content)

            time.sleep(2)

            logger = logging.getLogger("DOWNLOAD_FILES")
            logger.info('DOWNLOAD: @{}'.format(url))

        prev_species = sp
        curr_file += 1

        if curr % 20 == 0:
            print(str(curr), " of ", str(total[0]))

    logger = logging.getLogger("DOWNLOAD_FILES")
    logger.info('ALL FILES DOWNLOADED')


if __name__ == "__main__":
    # '''
    # We use this to index all docs written with spanish chars
    # '''
    # reload(sys)
    # sys.setdefaultencoding('ISO-8859-1')

    conf_parser = argparse.ArgumentParser(
        description=__doc__,  # printed with -h/--help
        # Don't mess with format of description
        formatter_class=argparse.RawDescriptionHelpFormatter,
        # Turn off help, so we print all options in response to -h
        add_help=False
    )

    conf_parser.add_argument("-fp", "--file_path",
                             help="Path of the file to read all corpus paragraphs", metavar="FILE")
    conf_parser.add_argument("-dp", "--dest_path",
                             help="Path of the file to read all corpus paragraphs", metavar="FILE")
    conf_parser.add_argument("-t", "--task",
                             help="Path of the file to read all corpus paragraphs", metavar="FILE")

    args, remaining_argv = conf_parser.parse_known_args()

    logging.basicConfig(filename=os.path.join(
        args.dest_path, 'log.log'), level=logging.DEBUG)

    if args.task.startswith("c"):
        crawl(args.file_path, args.dest_path)
    elif args.task.startswith("d"):
        download(args.file_path, args.dest_path)
    else:
        print("TASK NOT ALLOWED")
        exit(1)
