#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-
import sys

import argparse
import collections
import configparser
import logging
import numpy
import os
import pandas
import pydash

import loader

from bs4 import BeautifulSoup

from pdb import set_trace as bp

par_track = 0


def parse_document(doc, dest_path):
    elements = []
    children = doc.children

    for child in children:
        try:
            tag_name = child.name

            if not tag_name:
                continue

            if tag_name == "sec":
                temp_doc = parse_document(child, dest_path)
                elements += temp_doc
            else:
                processed = "NO SOPORTADO"
                ordinal = -1

                if tag_name == "p" or tag_name == "title":
                    processed = child.text
                    global par_track
                    par_track += 1
                    ordinal = par_track

                temp = [tag_name, str(child), processed, ordinal]
                elements.append(temp)
        except:
            print(sys.exc_info())

    return elements


def preprocess(track_path, corpus_path, dest_path):
    res_df = pandas.DataFrame()
    df = pandas.read_csv(track_path)
    total = numpy.asarray(df).shape[0]

    count = 1
    for doc in df.iterrows():
        try:
            doc_file = doc[1]["id"]
            doc_lines = ""

            with open(os.path.join(corpus_path, doc_file+".xml"), mode="r") as _f:
                doc_lines = _f.read()

            xmldoc = BeautifulSoup(doc_lines, 'html.parser')
            front = xmldoc.front
            body = xmldoc.body

            global par_track
            par_track = 0
            parsed_doc = parse_document(body, dest_path)
            docdf = pandas.DataFrame(parsed_doc,
                                     columns=["tag", "original", "processed", "ordinal"])
            docdf["id"] = doc_file
            docdf["url"] = doc[1]["download"]
            docdf["source"] = doc[1]["source"]
            docdf["title"] = doc[1]["title"]

            res_df = pandas.concat([res_df, docdf])

            if count % 50 == 0:
                logger = logging.getLogger("PROCESSED_FILES")
                logger.info('PROCESSED: @{} of {}'.format(
                    str(count), str(total)))

            count += 1
        except:
            # TODO: FILE NOT FOUND EXCEPTION
            logger = logging.getLogger("EXCEPTION")
            logger.info(sys.exc_info())

    res_df.to_csv(os.path.join(dest_path, "docs_processed.csv"),
                  sep=',',
                  encoding='utf-8')


if __name__ == "__main__":
    # '''
    # We use this to index all docs written with spanish chars
    # '''
    # reload(sys)
    # sys.setdefaultencoding('ISO-8859-1')

    conf_parser = argparse.ArgumentParser(
        description=__doc__,  # printed with -h/--help
        # Don't mess with format of description
        formatter_class=argparse.RawDescriptionHelpFormatter,
        # Turn off help, so we print all options in response to -h
        add_help=False
    )

    conf_parser.add_argument("-cp", "--corpus_path",
                             help="Path of the file to read all corpus paragraphs", metavar="FILE")
    conf_parser.add_argument("-dp", "--dest_path",
                             help="Path of the file to read all corpus paragraphs", metavar="FILE")
    conf_parser.add_argument("-tp", "--track_path",
                             help="Path of the file to read all corpus paragraphs", metavar="FILE")

    args, remaining_argv = conf_parser.parse_known_args()

    logging.basicConfig(filename=os.path.join(args.dest_path, 'log.log'),
                        level=logging.DEBUG)

    preprocess(args.track_path, args.corpus_path, args.dest_path)
