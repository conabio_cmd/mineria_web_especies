#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-
import sys

sys.path.append('../')
sys.path.append('../text_utils')

import text_utils
from text_utils import preprocessing
from text_utils import sources
from text_utils import utils

from sources import crawler
from utils import utils as u

Crawler = crawler.Crawler
utils = u
