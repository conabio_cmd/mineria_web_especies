#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utils import get_corpus_file
import urllib.request
import warnings
from gensim.utils import simple_preprocess
from gensim.test.utils import datapath
from gensim.models import CoherenceModel
from sklearn.model_selection import train_test_split
from pdb import set_trace as bp
from nltk.corpus import stopwords
import pyLDAvis.gensim
import pyLDAvis
import os
import pandas
import unidecode
import re
import gensim.corpora as corpora
import gensim
import spacy
import configparser
import datetime
import logging
import collections
import argparse
import sys
sys.path.append('../')
sys.path.append('../text_utils')


stop_words = stopwords.words('english')
stop_words.extend(['from', 'subject', 're', 'edu', 'use'])
warnings.filterwarnings("ignore")


def sent_to_words(sentences):
    for sentence in sentences:
        try:
            # deacc=True removes punctuations
            s = unidecode.unidecode(sentence)
            temp = gensim.utils.simple_preprocess(str(s), deacc=True)
            yield(temp)
        except Exception as e:
            raise


def remove_stopwords(texts):
    return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] for doc in texts]


def make_bigrams(bigram_mod, texts):
    return [bigram_mod[doc] for doc in texts]


def lemmatization(nlp, texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    """https://spacy.io/api/annotation"""
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent))
        texts_out.append(
            [token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out


def process_data(data_words):
    # Build the bigram models
    # higher threshold fewer phrases.

    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)

    # Ngram models
    bigram_mod = gensim.models.phrases.Phraser(bigram)

    data_words_nostops = remove_stopwords(data_words)
    data_words_bigrams = make_bigrams(bigram_mod, data_words_nostops)

    # Initialize spacy 'en' model, keeping only tagger component (for efficiency)
    nlp = spacy.load('en', disable=['parser', 'ner'])

    # Do lemmatization keeping only noun, adj, vb, adv
    data_lemmatized = data_words_bigrams
    # for dwb in data_words_bigrams:
    #     try:
    #         temp = lemmatization(nlp,
    #                              [dwb],
    #                              allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])
    #         data_lemmatized += temp
    #     except Exception as e:
    #         continue

    return data_lemmatized


def create_data_words(corpus_file):
    df_train = pandas.read_csv(corpus_file)

    plain_data = df_train["text"]

    data = [re.sub(r'[\(\)]', "", str(sent)) for sent in plain_data]
    # Mail
    data = [re.sub('\S*@\S*\s?', '', sent) for sent in data]

    # Line chars
    data = [re.sub('\s+', ' ', sent) for sent in data]

    # Single quotes
    data = [re.sub("\'", "", sent) for sent in data]

    # using nltk
    data = list(sent_to_words(data))

    return plain_data, data


def load_data(corpus_tag, dest_path):
    # TODO: Check if needed to save processed data
    '''
        PREPROCESSING
    '''
    res = []
    files_to_check = [corpus_tag + fs for fs in ["_train.csv", "_test.csv"]]

    start_time = datetime.datetime.now()
    for ftc in files_to_check:
        corpus_file = os.path.join(dest_path, ftc)
        logger = logging.getLogger("EXEC_TIME")
        logger.debug('PROCESSING: %s', ftc)
        get_corpus_file(ftc, dest_path)

        plain_data, data_words = create_data_words(corpus_file)
        data = process_data(data_words)

        data = zip(plain_data, data)
        res.append([d for d in data])

    time_elapsed = datetime.datetime.now() - start_time
    logger = logging.getLogger("EXEC_TIME")
    logger.info('PREPROCESSING DONE IN (hh:mm:ss.ms) @{}'.format(time_elapsed))

    return res[0], res[1]


class LDA():
    num_topics = 30
    passes = 20

    def __init__(self, dest_path,
                 num_topics,
                 passes,
                 model_path=None):
        self.model_path = model_path
        self.dest_path = dest_path
        self.num_topics = num_topics or self.num_topics
        self.passes = passes or self.passes
        self.model = None

    def create_resources(self, data):
        # Dictionary & TDF
        start_time = datetime.datetime.now()
        self.id2word = corpora.Dictionary(data)
        self.corpus = [self.id2word.doc2bow(text) for text in data]
        time_elapsed = datetime.datetime.now() - start_time
        logger = logging.getLogger("EXEC_TIME")
        logger.info('RESOURCES BUILT IN (hh:mm:ss.ms) @{}'.format(time_elapsed))

    def show_stats(self, data):
        topic_qty = len(self.model.print_topics(num_topics=-1))
        perplexity = self.model.log_perplexity(self.corpus)
        coherence_model_lda = CoherenceModel(model=self.model,
                                             texts=data,
                                             dictionary=self.id2word,
                                             coherence='c_v')

        print("TOPICOS:"+str(topic_qty))
        print("PERPLEJIDAD:"+str(perplexity))
        print("COHERENCIA:"+str(coherence_model_lda.get_coherence()))

        # SHOW VISTOPICS
        try:
            pyLDAvis.enable_notebook()
        except Exception as e:
            print(e)

        vis = pyLDAvis.gensim.prepare(self.model,
                                      self.corpus,
                                      self.id2word,
                                      mds='mmds')

        vis_file_name = os.path.join(self.dest_path, 'topics.html')
        print("TOPICS VISUALIZATION SAVED IN: ", vis_file_name)

        pyLDAvis.save_html(vis, vis_file_name)

    def fit(self):
        # Here we train the model with data
        start_time = datetime.datetime.now()

        if not self.model_path:
            print("TRAINING MODEL")
            self.model = gensim.models.ldamulticore.LdaMulticore(corpus=self.corpus,
                                                                 num_topics=self.num_topics,
                                                                 id2word=self.id2word,
                                                                 passes=self.passes)
            self.model.save(os.path.join(self.dest_path, "model"))
        else:
            if not os.path.isfile(self.model_path):
                raise Exception("MODEL NOT FOUND")
            else:
                print("MODEL FOUND, USING PREVIOUS")
                self.model = gensim.models.ldamodel.LdaModel.load(
                    self.model_path)

        time_elapsed = datetime.datetime.now() - start_time
        logger = logging.getLogger("EXEC_TIME")
        logger.info('MODEL BUILT IN (hh:mm:ss.ms) @{}'.format(time_elapsed))

    def transform(self, data, corpus):
         # Init output
        sent_topics_df = pandas.DataFrame()

        # Get main topic in each document
        for i, row in enumerate(self.model[corpus]):
            row = sorted(row, key=lambda x: (x[1]), reverse=True)

            for j, (topic_num, prop_topic) in enumerate(row):
                if j == 0:
                    wp = self.model.show_topic(topic_num)
                    topic_keywords = ", ".join([word for word, prop in wp])
                    sent_topics_df = sent_topics_df.append(
                        pandas.Series([int(topic_num), round(
                            prop_topic, 4), topic_keywords]),
                        ignore_index=True)

        sent_topics_df.columns = ['Topic', 'Contribution', 'Keywords']

        return sent_topics_df

    def fit_transform(self, data, stats=True):
        self.create_resources(data)
        self.fit()

        if stats:
            self.show_stats(data)

        return self.transform(data, self.corpus)


def lda(corpus_tag, dest_path, model_path, num_topics, passes):
    # Steps:
    # Load data
    train, test = load_data(corpus_tag, dest_path)

    # Convert data into feature vector
    model = LDA(dest_path, model_path, num_topics, passes)

    train = pandas.DataFrame(train)
    train.columns = ['text', 'X']

    test = pandas.DataFrame(test)
    test.columns = ['text', 'Y']

    # Fit and transforrm
    train_topics = model.fit_transform(train.X)
    train_topics["original"] = train.text

    train_topics_filepath = os.path.join(dest_path, "train_topics.csv")

    train_topics.to_csv(train_topics_filepath, sep=',', encoding='utf-8')
    print("TOPICS SAVED IN: ", train_topics_filepath)

    return train_topics


if __name__ == "__main__":
    '''
    We use this to index all docs written with spanish chars
    '''
    conf_parser = argparse.ArgumentParser(
        description=__doc__,  # printed with -h/--help
        # Don't mess with format of description
        formatter_class=argparse.RawDescriptionHelpFormatter,
        # Turn off help, so we print all options in response to -h
        add_help=False
    )

    conf_parser.add_argument("-ct", "--corpus_tag",
                             help="  ", metavar="FILE")
    conf_parser.add_argument("-dp", "--dest_path",
                             help="  ", metavar="FILE")
    conf_parser.add_argument("-mp", "--model_path",
                             help="  ", metavar="FILE")
    conf_parser.add_argument("-nt", "--num_topics",
                             help="  ", metavar="FILE")
    conf_parser.add_argument("-p", "--passes",
                             help="  ", metavar="FILE")

    args, remaining_argv = conf_parser.parse_known_args()

    # idp = args.is_data_processed or 0
    logging.basicConfig(filename=os.path.join(args.dest_path, 'log.log'),
                        level=logging.DEBUG)

    if "/" in args.corpus_tag:
        print("CORPUS TAG INVALID")
        exit(1)

    lda(corpus_tag=args.corpus_tag,
        dest_path=args.dest_path,
        model_path=args.model_path,
        num_topics=args.num_topics,
        passes=args.passes
        )
