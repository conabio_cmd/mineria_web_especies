#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-
import urllib.request
import os
from pdb import set_trace as bp


def download(corpus_id, dest_file, attempts=1):
    print("El conjunto de datos "+corpus_id+" no existe")
    print("User:")
    user = input()
    print("Pass:")
    passwrd = input()

    try:
        print("Intentando descargar: "+corpus_id)
        urllib.request.urlretrieve('ftp://'+user + ':'+passwrd +
                                   '@172.16.3.111/share/corpora/'+corpus_id,
                                   dest_file)
        print("Archivo descargado")
        return user, passwrd
    except urllib.error.URLError as exc:
        if "530" in exc.reason and attempts < 3:
            attempts += 1
            print("Credenciales incorrectas")
            download(corpus_id, dest_file, attempts)
        else:
            print("Demasiados intentos fallidos")
            input()
            raise
    except:
        raise


def get_corpus_file(corpus_tag, dest_path):
    corpus_file = os.path.join(dest_path, corpus_tag)

    # Check if file exists, if not try to download from FTP
    if not os.path.isfile(corpus_file):
        try:
            download(corpus_tag, dest_file=corpus_file)
            if not os.path.isfile(corpus_file):
                print("EL ARCHIVO NO PUDO SER DESCARGADO")
                return
        except:
            print("EL ARCHIVO NO PUDO SER DESCARGADO")
            bp()
            raise
