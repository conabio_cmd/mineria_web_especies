#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-
import sys

import argparse
import collections
import configparser
import logging
import os
import pandas
import pydash
import requests
import time
import threading

import loader

from pdb import set_trace as bp

Pool = loader.Pool
Post = loader.Post
preproc = loader.preprocess


class TrainCorpus(Pool):
    def __init__(self, dest_path, solr_url, solr_core,
                 threadNames=["t1"], onFinish=None):
        self.dest_path = dest_path
        self.solr_url = solr_url
        self.solr_core = solr_core
        # self.set_threads(threadNames)
        self.indexer = Post(solr_url)

        self.token_to_write = []
        self.species_to_write = []
        self.found = set()
        self.batch_size = 200
        self.prev = ""
        lines = []

        self.terms = []

        try:
            with open(os.path.join(self.dest_path, 'tokens.txt')) as terms_file:
                lines = terms_file.read()
                lines = lines.split("\n")

            self.terms = pydash.chain(lines)\
                .filter(lambda x: len(x) > 0)\
                .map(lambda x: x.replace("\n", "").strip())\
                .value()

        except FileNotFoundError:
            print("TOKENS FILE NOT FOUND")

    def setPool(self):
        all_batches = []

        # We retrieve species that are need for validation
        query = "q=species:* AND tag:p&fl=id"
        results = self.indexer.select(self.solr_core, query)
        num_found = results["response"]["numFound"]
        batches = [{"start": r, "total": num_found, "term": "*", "type": "species"}
                   for r in range(0, num_found, self.batch_size)]
        all_batches += batches

        # Also all batches referred to tokens on topics seed words
        for term in self.terms:
            query = "q=open_search:\""+term+"\" AND tag:p&fl=id"

            results = self.indexer.select(self.solr_core, query)
            num_found = results["response"]["numFound"]

            batches = [{"start": r, "total": num_found, "term": term, "type": "tokens"}
                       for r in range(0, num_found, self.batch_size)]
            all_batches += batches

        logger = logging.getLogger("DOCUMENTS")
        logger.info('BATCHES TO PROCESS: {}'.format(str(len(all_batches))))

        return all_batches

    def process(self, thread, **kwargs):
        start = kwargs["start"]
        num_found = kwargs["total"]
        term = kwargs["term"]
        type_query = kwargs["type"]

        if type_query == "species":
            query = "fl=id,open_search,ordinal&q=species:* AND tag:p&rows={{2}}&start={{3}}"

            query = query.replace("{{2}}", str(self.batch_size))\
                .replace("{{3}}", str(start))

            results = self.indexer.select(self.solr_core, query)
            docs = results["response"]["docs"]

            for doc in docs:
                id = doc["id"]
                if id in self.found:
                    continue

                self.found.add(id)
                processed = " ".join(preproc.process_line(doc["open_search"]))

                if "\n" in processed:
                    bp()
                stem = " ".join(preproc.lemmatization(processed))

                self.species_to_write.append(
                    [id, doc["open_search"], processed, stem, doc["ordinal"]]
                )
        else:
            query = "fl=id,open_search,ordinal&q=open_search:{{1}} AND tag:p&rows={{2}}&start={{3}}"

            query = query.replace("{{1}}", term)\
                .replace("{{2}}", str(self.batch_size))\
                .replace("{{3}}", str(start))

            results = self.indexer.select(self.solr_core, query)
            docs = results["response"]["docs"]

            for doc in docs:
                id = doc["id"]
                if id in self.found:
                    continue

                self.found.add(id)
                processed = " ".join(preproc.process_line(doc["open_search"]))
                stem = preproc.lemmatization(processed)

                self.token_to_write.append(
                    [id, doc["open_search"], processed, stem, doc["ordinal"]]
                )

        logger = logging.getLogger("DOCUMENTS")
        logger.info('PROCESSED: @{} of {} of {}'.format(
            str(start), str(num_found), term))

    def onFinish(self):
        print("Total", len(self.token_to_write))

        df = pandas.DataFrame(self.token_to_write,
                              columns=["id", "text", "processed", "stem", "ordinal"])
        df.to_csv(os.path.join(self.dest_path, "tokens.csv"),
                  sep=',', encoding='utf-8')

        print("Total", len(self.species_to_write))

        df = pandas.DataFrame(self.species_to_write,
                              columns=["id", "text", "processed", "stem", "ordinal"])
        df.to_csv(os.path.join(self.dest_path, "species.csv"),
                  sep=',', encoding='utf-8')


if __name__ == "__main__":
    '''
    We use this to index all docs written with spanish chars
    '''
    conf_parser = argparse.ArgumentParser(
        description=__doc__,  # printed with -h/--help
        # Don't mess with format of description
        formatter_class=argparse.RawDescriptionHelpFormatter,
        # Turn off help, so we print all options in response to -h
        add_help=False
    )

    conf_parser.add_argument("-dp", "--dest_path",
                             help="Destination path of corpus", metavar="FILE")
    conf_parser.add_argument("-u", "--url",
                             help="Destination path of corpus", metavar="FILE")
    conf_parser.add_argument("-c", "--core",
                             help="Destination path of corpus", metavar="FILE")

    args, remaining_argv = conf_parser.parse_known_args()

    logging.basicConfig(filename=os.path.join(args.dest_path, 'log.log'),
                        level=logging.DEBUG)

    tc = TrainCorpus(dest_path=args.dest_path,
                     solr_url=args.url,
                     solr_core=args.core)

    tc.start()
