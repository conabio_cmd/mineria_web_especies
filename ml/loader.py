#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-
import sys

sys.path.append('../')
sys.path.append('../text_utils')

import text_utils
from text_utils import indexing
from text_utils import utils
from text_utils import sources
from text_utils import preprocessing
from preprocessing import preprocess
from preprocessing import corpus
from indexing import solr_post

from utils import pool
from utils import utils as u


preprocess = preprocess
Vocab = corpus.Vocab
utils = u
Pool = pool.Pool
Post = solr_post.SolrPost