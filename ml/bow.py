#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import urllib.request
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure
from sklearn.manifold import TSNE
from gensim.models.keyedvectors import KeyedVectors
import numpy
from gensim.models import Word2Vec
import warnings
from sklearn.model_selection import train_test_split
import loader
import pandas
from pdb import set_trace as bp
import datetime
import os
import warnings
import logging
import collections
import argparse
import networkx as nx
import sys
import json
from ast import literal_eval

import pydash

from lutils import get_corpus_file
sys.path.append('../')
sys.path.append('../text_utils')

preprocess = loader.preprocess
warnings.filterwarnings("ignore")


conf = {
    "corpus_tag": None,
    "dest_path": None,
    "model_path": None,
    "window": 8,
    "epochs": 10,
    "use_stem": 0
}


def download(corpus_id):
    print("El conjunto de datos no existe")
    print("User:")
    user = input()

    print("Pass:")
    passwrd = input()

    print("Descargando conjunto de datos")
    urllib.request.urlretrieve('ftp://'+user + ':'+passwrd +
                               '@172.16.3.111/share/corpora/'+corpus_id,
                               "./"+corpus_id)
    print("Archivo descargado")


def load_data(corpus_tag, dest_path):
    # TODO: Check if needed to save processed data
    '''
        PREPROCESSING
    '''
    res = []

    files_to_check = [corpus_tag + fs for fs in ["_train.csv", "_test.csv"]]

    start_time = datetime.datetime.now()

    for ftc in files_to_check:
        conv = []
        corpus_file = os.path.join(dest_path, ftc)
        logger = logging.getLogger("EXEC_TIME")
        logger.debug('PROCESSING: %s', ftc)
        get_corpus_file(ftc, dest_path)

        df = pandas.read_csv(corpus_file)
        res.append(df)

        # for row in df.iterrows():
        #     conv.append([
        #         row["id"],
        #         row["text"],
        #         row["processed"],
        #         " ".join(literal_eval(row["stem"])),
        #         row["ordinal"]
        #     ])

        # df = pandas.DataFrame(sopa, columns=['id', 'texts', 'processed', 'stem', 'ordinal'])
        # df.to_csv(os.path.join("/Users/rrivera/Desktop", ftc),sep=',', encoding='utf-8')

    time_elapsed = datetime.datetime.now() - start_time
    logger = logging.getLogger("EXEC_TIME")
    logger.info('PREPROCESSING DONE IN (hh:mm:ss.ms) @{}'.format(time_elapsed))

    return res[0], res[1]


def model_stats(model, tokens):
    samples = numpy.empty((0, 300), dtype='f')
    labels = []

    topics = []
    for token in tokens:
        try:
            samples = numpy.append(samples,
                                   numpy.array([model[token]]), axis=0)

            labels.append(token)
            close_words = model.similar_by_word(token)
            topics.append([token, str(close_words)])

            for word in close_words:
                vector = model[word[0]]
                labels.append(word[0])
                samples = numpy.append(samples,
                                       numpy.array([vector]), axis=0)

        except KeyError as err:
            # TODO: log blank
            print(err)
    # tsne
    tsne = TSNE(perplexity=40,
                n_components=2,
                init='pca',
                n_iter=250,
                random_state=23)
    Y = tsne.fit_transform(samples)

    x_coords = Y[:, 0]
    y_coords = Y[:, 1]

    plt.scatter(x_coords, y_coords)

    for label, x, y in zip(labels, x_coords, y_coords):
        plt.annotate(label,
                     xy=(x, y),
                     xytext=(0, 0), textcoords='offset points')

    plt.xlim(x_coords.min()+0.00005, x_coords.max()+0.00005)
    plt.ylim(y_coords.min()+0.00005, y_coords.max()+0.00005)
    plt.show()

    df = pandas.DataFrame(topics, columns=['Topic word', 'Close words'])
    return df


class Vectorizer():
    window = 8
    epochs = 20

    def __init__(self, dest_path, model_path=None, epochs=20, window=8):
        self.model_path = model_path
        self.dest_path = dest_path
        self.model = None
        self.topics = None
        self.vocab = None

        self.epochs = int(epochs or self.epochs)
        self.window = int(window or self.window)

    def get_model(self):
        return self.model

    def extend(self, extend, searched, level=1):
        edges = []
        to_extend = list([e for e in extend])
        already = list(searched)
        if level == 2:
            return []

        for ext in extend:
            if ext in searched:
                continue

            already.append(ext)
            try:
                close_words = self.model.similar_by_word(ext)
                print(ext+" --> " + str([cw[0] for cw in close_words]))
                for cw in close_words:
                    edges.append((ext, cw))
                    to_extend.append(cw[0])
            except:
                print(sys.exc_info())

        already = set(already)
        to_extend = set(to_extend)
        level += 1
        edges += self.extend(to_extend, already, level=level)

        edges_to_draw = [(edge[0], edge[1][0]) for edge in edges]
        edges = [edge[1] for edge in edges]

        return edges_to_draw, edges

    def extend_vocab(self, draw_vocab=True):
        pnodes = []
        snodes = []
        topics = []
        res = []

        with open("./seed_words.txt") as sw:
            lines = sw.readlines()
            for line in lines:
                line = line.replace("\n", "")
                topic, aux = line.split("->")

                topic = topic.strip()
                pnodes.append(("_ROOT", topic))
                curr_topic = []

                for a in aux.split(","):
                    a = a.lower().strip()
                    pnodes.append((topic, a))
                    curr_topic.append((a, 1))

                topics.append(((topic, 1), curr_topic))

        for topic in topics:
            to_extend = [topic[0][0]]+[tt[0] for tt in topic[1]]

            to_draw, extended_edges = self.extend(
                to_extend, set())

            snodes += to_draw
            temp = [topic[0]] + topic[1] + extended_edges

            tokens = (topic[0][0], temp)
            res.append(tokens)

        labels = dict([(xx, xx) for xx in pnodes])
        labels.update(dict([(xx, xx) for xx in snodes]))

        return res, pnodes, snodes

    def fit(self, data):
        # Here we train the model with data
        start_time = datetime.datetime.now()

        if not self.model_path:
            print("TRAINING MODEL")
            self.model = Word2Vec(min_count=1,
                                  window=self.window,
                                  workers=4,
                                  size=300)
            self.model.build_vocab(data)
            self.model.train(data,
                             total_examples=self.model.corpus_count,
                             epochs=self.epochs)

            model_path = os.path.join(self.dest_path, "w2v")
            self.model.wv.save_word2vec_format(model_path, binary=False)
            print("TRAINED")
        else:
            if not os.path.isfile(self.model_path):
                raise Exception("MODEL NOT FOUND")
            else:
                print("MODEL FOUND, USING PREVIOUS")
                self.model = KeyedVectors.load_word2vec_format(self.model_path)

        time_elapsed = datetime.datetime.now() - start_time
        logger = logging.getLogger("EXEC_TIME")
        logger.info('MODEL BUILT IN (hh:mm:ss.ms) @{}'.format(time_elapsed))

    def transform(self, data):
        dummy = self.model.wv.index2entity[0]
        v = self.model.wv.get_vector(dummy)
        self.D = v.shape[0]
        X = [numpy.zeros((len(data), self.D)),
             numpy.zeros((len(data), self.D))]
        n = 0

        topics = []

        for t in self.topics:
            topics.append(dict(t[1]))

        for paragraph in data:
            curr_topic = 0
            for topic in topics:
                tokens = paragraph.split()
                vecs = []
                hits = [numpy.zeros(self.D)]
                m = 0

                for word in tokens:
                    try:
                        vec = self.model.wv.get_vector(word)
                        vecs.append(vec)
                        if word in topic:
                            hits.append(vec)
                        m += 1
                    except KeyError:
                        pass

                if len(vecs) > 0:
                    try:
                        vecs = numpy.array(vecs)
                        hits = numpy.array(hits)
                        X[curr_topic][n] = hits.sum(axis=0)/vecs.sum(axis=0)
                    except:
                        print(sys.exc_info())
                        print("ERR")

                else:
                    # TODO: log bl
                    pass

                curr_topic += 1

            n += 1

        return X

    def fit_transform(self, data, stats=True):
        # TODO: We hace to check how to split paragraphs
        train_data = [d.split() for d in data]
        self.fit(train_data)

        topics, pedges, sedges = self.extend_vocab()
        self.topics = topics

        if stats:
            self.draw(pedges, sedges)

        return self.transform(data)

    def draw(self, pedges, sedges):
        figure(num=None, figsize=(8, 6))

        edges_to_draw = pedges + sedges

        pnodes = [e[1] for e in pedges]
        snodes = [e[1] for e in sedges]
        labels = dict([(xx, xx) for xx in pnodes])
        labels.update(dict([(xx, xx) for xx in snodes]))

        G = nx.Graph()
        G.add_nodes_from(pnodes)
        G.add_nodes_from(snodes)
        G.add_edges_from(edges_to_draw)

        pos = nx.spring_layout(G)
        nx.draw_networkx_nodes(G, pos, nodelist=pnodes)
        nx.draw_networkx_nodes(G, pos, nodelist=snodes,
                               node_color='b', node_size=100, alpha=0.4)
        nx.draw_networkx_edges(G, pos, alpha=0.5, edge_color='b')
        nx.draw_networkx_labels(G, pos, labels, font_size=9)

        plt.show()


# def bow(corpus_tag, dest_path, model_path, window, epochs, use_stem):
def bow(config_file):
    try:
        curr = os.path.split(os.path.realpath(__file__))
        config_file_id = os.path.splitext(curr[1])[0]+"_config"

        for key in conf.keys():
            conf[key] = config_file.get(key, None) or conf[key]

        json.dump(conf, open(os.path.join(
            conf["dest_path"], config_file_id), 'w'))

        train, test = load_data(conf["corpus_tag"], conf["dest_path"])

        if int(conf["use_stem"]):
            train.processed = train.stem

        vectorizer = Vectorizer(conf["dest_path"], conf["model_path"],
                                epochs=conf["epochs"],
                                window=conf["window"])
        X = vectorizer.fit_transform(train.processed)
        Y = vectorizer.transform(test.processed)

    except KeyError as e:
        print("Malformed config file")
        raise e


if __name__ == "__main__":
    '''
    We use this to index all docs written with spanish chars
    '''
    curr = os.path.split(os.path.realpath(__file__))
    config_file = os.path.splitext(curr[1])[0]+"_config"
    config_file = os.path.join(curr[0], config_file)

    if not os.path.isfile(config_file):
        print("CONFIG FILE NOT FOUND")
        exit(1)

    config_file = json.load(open(config_file))

    try:
        if not config_file.get("dest_path", None):
            print("DESTINATION PATH IS NEEDED")

        if not config_file.get("corpus_tag", None):
            print("CORPUS TAGFILE IS NEEDED")

        logging.basicConfig(filename=os.path.join(config_file["dest_path"], 'log.log'),
                            level=logging.DEBUG)

        bow(config_file)
    except KeyError as e:
        print("Malformed config file")
        exit(1)

    # corpus_tag = config_file["corpus_tag"]

    # conf_parser = argparse.ArgumentParser(
    #     description=__doc__,  # printed with -h/--help
    #     # Don't mess with format of description
    #     formatter_class=argparse.RawDescriptionHelpFormatter,
    #     # Turn off help, so we print all options in response to -h
    #     add_help=False
    # )

    # conf_parser.add_argument("-ct", "--corpus_tag",
    #                          help="Posible values: tokens  ", metavar="FILE")
    # conf_parser.add_argument("-dp", "--dest_path",
    #                          help="Destination path where results will be stores ", metavar="FILE")
    # conf_parser.add_argument("-mp", "--model_path",
    #                          help="If a pretrained model is available, put its path ", metavar="FILE")

    # # W2V trainables
    # conf_parser.add_argument("-us", "--use_stem",
    #                          help="If is set to true stem field of corpus will be used", metavar="FILE")
    # conf_parser.add_argument("-w", "--window",
    #                          help="Distance between the current and predicted word within a sentence.  ", metavar="FILE")
    # conf_parser.add_argument("-e", "--epochs",
    #                          help="Number of epochs over the corpus ", metavar="FILE")

    # args, remaining_argv = conf_parser.parse_known_args()
