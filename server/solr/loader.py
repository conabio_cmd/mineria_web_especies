import sys

sys.path.append('../../')
sys.path.append('../../text_utils')

import text_utils
from text_utils import preprocessing
from text_utils import utils
from text_utils import indexing

from indexing import solr_post
from utils import pool

solr_post = solr_post
Pool = pool.Pool