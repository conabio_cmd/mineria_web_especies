#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-
from pdb import set_trace as bp
import sys

import argparse
import collections
import configparser
import logging
import numpy
import os
import pandas
import requests
import time
import threading

import loader

solr_post = loader.solr_post
Pool = loader.Pool


class Indexer(Pool):
    batch_size = 200
    step = 1

    def __init__(self, corpus_path, url, core):
        self.indexer = solr_post.SolrPost(url)
        self.core = core
        self.corpus_path = corpus_path
        self.set_threads(["t1", "t2"])

    def setPool(self):
        df = pandas.read_csv(self.corpus_path)

        documents = [doc for doc in df.iterrows()]
        self.doc_qty = len(documents)

        batches = [{"docs": documents[r: r + self.batch_size]}
                   for r in range(0, self.doc_qty, self.batch_size)]

        logger = logging.getLogger("INDEXING")
        logger.info('DOCUMENTS TO INDEX: @{}'.format(str(self.doc_qty)))
        return batches

    def process(self, thread, **kwargs):
        docs = kwargs["docs"]

        logger = logging.getLogger("INDEXING")
        logger.info('INDEXED: @{}'.format(str(self.step)))

        for doc in docs:            
            self.import_document(doc[1])
            self.step += 1

    def onFinish(self):
        logger = logging.getLogger("INDEXING")
        logger.info('DONE')
        print("EVERYTHING IN PLACE")

    def import_document(self, doc):
        try:
            data = {'file_id': doc["id"],
                    'tag': doc["tag"],
                    'source': doc["source"],
                    'title': doc["title"],
                    'original':  doc["original"],
                    'content': doc["processed"],
                    'ordinal': doc["ordinal"]}


            self.indexer.post_text(self.core, data)
        except:
            print(sys.exc_info())
            bp()
            pass


if __name__ == "__main__":
    conf_parser = argparse.ArgumentParser(
        description=__doc__,  # printed with -h/--help
        # Don't mess with format of description
        formatter_class=argparse.RawDescriptionHelpFormatter,
        # Turn off help, so we print all options in response to -h
        add_help=False
    )

    conf_parser.add_argument("-cp", "--corpus_path",
                             help="Path of the file to read all corpus paragraphs", metavar="FILE")
    conf_parser.add_argument("-u", "--url",
                             help="Path of the file to read all corpus paragraphs", metavar="FILE")
    conf_parser.add_argument("-c", "--core",
                             help="Path of the file to read all corpus paragraphs", metavar="FILE")

    args, remaining_argv = conf_parser.parse_known_args()

    logging.basicConfig(filename=os.path.join('./log.log'),
                        level=logging.DEBUG)

    i = Indexer(args.corpus_path, args.url, args.core)
    i.start()
